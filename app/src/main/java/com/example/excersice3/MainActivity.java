package com.example.excersice3;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.excersice3.model.ItemData;
import com.example.excersice3.presenter.ItemComperator;
import com.example.excersice3.presenter.MainPresenter;
import com.example.excersice3.presenter.RecyclerItemListener;
import com.example.excersice3.presenter.RecyclerItemTouchHelper;
import com.example.excersice3.view.ItemAdapter;
import com.example.excersice3.view.ItemDataINF;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MainActivity extends AppCompatActivity implements ItemDataINF, RecyclerItemListener {
    Toolbar mToolbar;
    MainPresenter mMainPresenter;
    RecyclerView mRecyclerView;
    List<ItemData> mItemData;
    ItemAdapter mItemAdapter;
    ItemComperator mComperator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        intitView();
    }

    private void intitView() {
        mItemData = new ArrayList<>();
        mRecyclerView = findViewById(R.id.recyclerview);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mItemAdapter = new ItemAdapter(mRecyclerView, mItemData, getApplicationContext(), this);
        mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        mRecyclerView.setAdapter(mItemAdapter);
        mMainPresenter = new MainPresenter(mItemData, getApplicationContext());
        mMainPresenter.CreateData();
        mComperator = new ItemComperator();

        ItemTouchHelper.SimpleCallback simpleCallback = new RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT, this);
        new ItemTouchHelper((simpleCallback)).attachToRecyclerView(mRecyclerView);

    }

    @Override
    public void onLongClick(int position) {
        int id = mItemData.get(position).getId() + 1;
        int pos = position;
        String title = "Item {" + (pos + 1) + "} - With ID = {" + (id) + "}";
        ItemData itemData = new ItemData(id, pos + 1, title, mItemData.get(pos).isCheck());
        mItemData.add(itemData);
        mItemAdapter.notifyItemInserted(pos);
        for (int i = pos + 1; i < mItemData.size(); i++) {
            int newid = mItemData.get(i).getId() + 1;
            int newPos = i + 1;
            String newtitle = "Item {" + (i + 1) + "} - With ID = {" + (mItemData.get(i).getId()) + "}";
            ItemData itemData2 = new ItemData(newid, newPos, newtitle, mItemData.get(i).isCheck());
            mItemData.set(i, itemData2);
            mItemAdapter.notifyItemChanged(i);
        }
    }

    @Override
    public void loadMore() {
        if (mItemData.size() < 40) {
            mItemData.add(null);
            mItemAdapter.notifyItemInserted(mItemData.size() - 1);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mItemData.remove(mItemData.size() - 1);
                    int scrollPosition = mItemData.size();
                    mItemAdapter.notifyItemRemoved(mItemData.size());
//                    List<Integer> newList = mMainPresenter.noRepeat(41,99);
                    for (int i = 0; i < scrollPosition; i++) {
                        int newid = mItemData.get(i).getId() + 1;
                        int newPos = scrollPosition + i + 1;
                        String newtitle = "Item {" + (scrollPosition + 1 + i) + "} - With ID = {" + (mItemData.get(i).getId() + 1) + "}";
                        ItemData itemData2 = new ItemData(newid, newPos, newtitle, mItemData.get(i).isCheck());
                        mItemData.add(itemData2);
                    }

                    mItemAdapter.notifyDataSetChanged();
                    mItemAdapter.setLoaded();
                }
            }, 3000);//Time to load

        } else {
            Toast.makeText(MainActivity.this, "Load data completed !", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.mymenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                View view = LayoutInflater.from(this).inflate(R.layout.dialog, null);
                AlertDialog.Builder dialog = new AlertDialog.Builder(this);
                dialog.setTitle(R.string.dialog_search);
                dialog.setView(view);
                final EditText input = view.findViewById(R.id.edt_input);
                final RadioGroup radioGroup = view.findViewById(R.id.radio_group);
                radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                        checkedId = radioGroup.getCheckedRadioButtonId();
                        if (checkedId == R.id.search_by_id) {
                            input.setEnabled(true);
                            input.setInputType(InputType.TYPE_CLASS_NUMBER);
                        } else if (checkedId == R.id.search_by_title) {
                            input.setEnabled(true);
                            input.setInputType(InputType.TYPE_CLASS_TEXT);
                        } else if (checkedId == R.id.search_id_max) {
                            input.setEnabled(false);
                        } else if (checkedId == R.id.search_id_min) {
                            input.setEnabled(false);
                        } else if (checkedId == R.id.sort_list_increase) {
                            input.setEnabled(false);
                        } else if (checkedId == R.id.sort_list_decrease) {
                            input.setEnabled(false);
                        } else if (checkedId == R.id.search_progress_only_one) {
                            input.setEnabled(false);
                        } else if (checkedId == R.id.search_progress_more_one) {
                            input.setEnabled(false);
                        }
                    }
                });
                dialog.setPositiveButton(R.string.Ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String text = input.getText().toString();
                        List<ItemData> newList = new ArrayList<>();
                        List<Integer> list = new ArrayList<>();
                        for (int i = 0; i < mItemData.size(); i++) {
                            int progress = mItemData.get(i).getProgress();
                            list.add(progress);
                        }
                        ItemData itemData;
                        int selectedId = radioGroup.getCheckedRadioButtonId();
                        switch (selectedId) {
                            case R.id.search_by_id:
                                if (text != null || text.length() > 0) {
                                    for (int i = 0; i < mItemData.size(); i++) {
                                        if (mItemData.get(i).getId() == Integer.parseInt(text)) {
                                            itemData = new ItemData(mItemData.get(i).getId(), mItemData.get(i).getPosition(), mItemData.get(i).getTitle(), mItemData.get(i).isCheck());
                                            newList.add(itemData);
                                        }
                                    }
                                }
                                mItemData.clear();
                                mItemData.addAll(newList);
                                mItemAdapter.notifyDataSetChanged();
                                break;
                            case R.id.search_by_title:
                                if (text != null || text.length() > 0) {
                                    for (int i = 0; i < mItemData.size(); i++) {
                                        if (mItemData.get(i).getTitle().contains(text)) {
                                            itemData = new ItemData(mItemData.get(i).getId(), mItemData.get(i).getPosition(), mItemData.get(i).getTitle(), mItemData.get(i).isCheck());
                                            newList.add(itemData);
                                        }
                                    }
                                }
                                mItemData.clear();
                                mItemData.addAll(newList);
                                mItemAdapter.notifyDataSetChanged();
                                break;
                            case R.id.search_id_max:
                                itemData = Collections.max(mItemData, mComperator);
                                mItemData.clear();
                                mItemData.add(itemData);
                                mItemAdapter.notifyDataSetChanged();
                                break;
                            case R.id.search_id_min:
                                itemData = Collections.min(mItemData, mComperator);
                                mItemData.clear();
                                mItemData.add(itemData);
                                mItemAdapter.notifyDataSetChanged();
                                break;
                            case R.id.sort_list_increase:
                                Collections.sort(mItemData, mComperator);
                                mItemAdapter.notifyDataSetChanged();
                                break;
                            case R.id.sort_list_decrease:
                                Collections.reverse(mItemData);
                                mItemAdapter.notifyDataSetChanged();
                                break;
                            case R.id.search_progress_only_one:
                                for (int i =0;i<mItemData.size();i++){
                                    if (Collections.frequency(list,list.get(i))==1){
                                        itemData = new ItemData(mItemData.get(i).getId(), mItemData.get(i).getPosition(), mItemData.get(i).getTitle(), mItemData.get(i).isCheck());
                                        newList.add(itemData);
                                    }
                                }
                                mItemData.clear();
                                mItemData.addAll(newList);
                                mItemAdapter.notifyDataSetChanged();
                                break;
                            case R.id.search_progress_more_one:
                                for (int i =0;i<mItemData.size();i++){
                                    if (Collections.frequency(list,list.get(i))>1){
                                        itemData = new ItemData(mItemData.get(i).getId(), mItemData.get(i).getPosition(), mItemData.get(i).getTitle(), mItemData.get(i).isCheck());
                                        newList.add(itemData);
                                    }
                                }
                                mItemData.clear();
                                mItemData.addAll(newList);
                                mItemAdapter.notifyDataSetChanged();
                                break;
                        }
                    }
                });
                dialog.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                dialog.show();
                break;
            case R.id.home:
                onBackPressed();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int adapterPosition) {
        if (viewHolder instanceof ItemAdapter.ViewHolder) {
            int id = mItemData.get(viewHolder.getAdapterPosition()).getId();
            final int deleteIndex = viewHolder.getAdapterPosition();
            mItemAdapter.removeItem(deleteIndex);
            mItemAdapter.notifyDataSetChanged();
        }
    }
}
