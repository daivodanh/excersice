package com.example.excersice3.view;

public interface ItemDataINF {

    void onLongClick(int position);

    void loadMore();
}
