package com.example.excersice3.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.excersice3.R;
import com.example.excersice3.model.ItemData;
import com.example.excersice3.presenter.MainPresenter;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ItemAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<ItemData> mItemData;
    private Context mContext;
    private ItemDataINF mItemDataINF;
    private static final int VIEW_TYPE_LOADING = 1;
    private static final int VIEW_TYPE_ITEM = 0;
    private int visibleThereHold = 5;
    private int lastVisibleItem, totalitemCount;
    boolean isLoading;
    MainPresenter mMainPresenter;

    public ItemAdapter(RecyclerView mRecyclerView, List<ItemData> itemData, Context context, final ItemDataINF itemDataINF) {
        mItemData = itemData;
        mContext = context;
        mItemDataINF = itemDataINF;
        final LinearLayoutManager ll = (LinearLayoutManager) mRecyclerView.getLayoutManager();
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                totalitemCount = ll.getItemCount();
                lastVisibleItem = ll.findLastVisibleItemPosition();
                if (!isLoading && totalitemCount <= (lastVisibleItem + visibleThereHold)) {
                    if (itemDataINF != null) {
//                        itemDataINF.loadMore();
                    }
                    isLoading = true;
                }
            }
        });
        mMainPresenter = new MainPresenter(mItemData,mContext);
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            View view = inflater.inflate(R.layout.list_row_item, parent, false);
            return new ViewHolder(view);
        } else if (viewType == VIEW_TYPE_LOADING) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            View view1 = inflater.inflate(R.layout.list_item_loading, parent, false);
            return new LoadingView(view1);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof ViewHolder) {
            ((ViewHolder) holder).txt_position.setText(mItemData.get(position).getPosition() + "");
            ((ViewHolder) holder).txt_title.setText(mItemData.get(position).getTitle());
            ((ViewHolder) holder).mCheckBox.setOnCheckedChangeListener(null);
            ((ViewHolder) holder).mCheckBox.setChecked(mItemData.get(position).isCheck());
            ((ViewHolder) holder).mCheckBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mItemData.get(position).isCheck() == true) {
                        mItemData.get(position).setCheck(false);
                    } else {
                        mItemData.get(position).setCheck(true);
                    }
                }
            });
            holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    mItemDataINF.onLongClick(position);
                    return false;
                }
            });
            ((ViewHolder) holder).mProgressBar.setIndeterminate(false);
            ((ViewHolder) holder).mProgressBar.setProgress(mMainPresenter.getRandom(1,10));
        } else if (holder instanceof LoadingView) {
            ((LoadingView) holder).mProgressBar.setIndeterminate(true);
        }
    }


    @Override
    public int getItemCount() {
        return mItemData.size();
    }

    public void removeItem(int position) {
        mItemData.remove(position);
        notifyItemRemoved(position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txt_position, txt_title;
        CheckBox mCheckBox;
        LinearLayout mLayout;
        ProgressBar mProgressBar;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_position = itemView.findViewById(R.id.txt_position);
            txt_title = itemView.findViewById(R.id.txt_title);
            mCheckBox = itemView.findViewById(R.id.check);
            mLayout = itemView.findViewById(R.id.linear);
            mProgressBar = itemView.findViewById(R.id.progressBar);
        }
    }

    public class LoadingView extends RecyclerView.ViewHolder {
        ProgressBar mProgressBar;

        public LoadingView(@NonNull View itemView) {
            super(itemView);
            mProgressBar = itemView.findViewById(R.id.progress_bar);
        }
    }

    public int getItemViewType(int postition) {
        return mItemData.get(postition) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }
    public void setLoaded() {
        isLoading = false;
    }

}
