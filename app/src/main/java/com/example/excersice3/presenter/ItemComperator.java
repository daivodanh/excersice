package com.example.excersice3.presenter;

import com.example.excersice3.model.ItemData;

import java.util.Comparator;

public class ItemComperator implements Comparator<ItemData> {

    @Override
    public int compare(ItemData o1, ItemData o2) {
        //Sort Item by ID ASC
        if (o1.getId()<o2.getId()){
            return -1;
        }else if (o1.getId()==o2.getId()){
            return 0;
        }else {
            return 1;
        }
    }


}
