package com.example.excersice3.presenter;

import androidx.recyclerview.widget.RecyclerView;

public interface RecyclerItemListener {
    void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int adapterPosition);
}
