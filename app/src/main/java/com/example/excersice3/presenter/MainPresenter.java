package com.example.excersice3.presenter;

import android.content.Context;

import com.example.excersice3.model.ItemData;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MainPresenter {
    private List<ItemData> mList;
    private Context mContext;

    public MainPresenter(List<ItemData> list, Context context) {
        mList = list;
        mContext = context;
    }

    public void CreateData() {
        List<Integer> list = noRepeat(0, 99);
        for (int i = 0; i < 10; i++) {
            int id = list.get(i);
            int position = i + 1;
            String title = "Item{" + position + "} - With ID ={" + id + "}";
            int progress = getRandom(1, 10);
            ItemData itemData = new ItemData(id, position, title, false, progress);
            mList.add(itemData);
        }

    }

    public int getRandom(int min, int max) {
        Random random = new Random();
        int rd = random.nextInt(max - min) + min;
        return rd;
    }

    public ArrayList<Integer> noRepeat(int min, int max) {
        ArrayList<Integer> arrayList = new ArrayList<>();
        while (arrayList.size() < 10) {
            int random = getRandom(min, max);
            if (!arrayList.contains(random))
                arrayList.add(random);
        }
        return arrayList;
    }


}
