package com.example.excersice3.model;

import java.io.Serializable;

public class ItemData implements Serializable {
    private int id;
    private int position;
    private String title;
    private boolean isCheck;
    private int progress;

    public ItemData(int id, int position, String title, boolean isCheck) {
        this.id = id;
        this.position = position;
        this.title = title;
        this.isCheck = isCheck;
    }

    public ItemData(int id, int position, String title, boolean isCheck, int progress) {
        this.id = id;
        this.position = position;
        this.title = title;
        this.isCheck = isCheck;
        this.progress = progress;
    }

    public int getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        this.progress = progress;
    }

    public ItemData() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isCheck() {
        return isCheck;
    }

    public void setCheck(boolean check) {
        isCheck = check;
    }
}
